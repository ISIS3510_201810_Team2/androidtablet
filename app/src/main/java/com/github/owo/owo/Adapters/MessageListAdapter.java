package com.github.owo.owo.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.owo.owo.Activities.R;
import com.github.owo.owo.Models.OwoMessage;
import java.util.List;

public class MessageListAdapter extends RecyclerView.Adapter {

    private Context context;
    private List<OwoMessage> messageList;

    public MessageListAdapter(Context context, List<OwoMessage> messageList) {
        this.context = context;
        this.messageList = messageList;
    }

    @Override
    public int getItemCount() {
        return messageList.size();
    }

    @Override
    public int getItemViewType(int position) {
        OwoMessage message = messageList.get(position);
        return message.getMessageType();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == OwoMessage.MESSAGE_SENT) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_message_sent, parent, false);
            return new MessageHolder(view);
        } else if (viewType == OwoMessage.MESSAGE_RECEIVED) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_message_received, parent, false);
            return new MessageHolder(view);
        } else {
            return null;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        OwoMessage message = messageList.get(position);
        if (message.getMessageType() == OwoMessage.MESSAGE_SENT) {
            ((MessageHolder) holder).bind(message);
        } else if (message.getMessageType() == OwoMessage.MESSAGE_RECEIVED) {
            ((MessageHolder) holder).bind(message);
        }
    }

    private class MessageHolder extends RecyclerView.ViewHolder {
        TextView messageText;

        MessageHolder(View itemView) {
            super(itemView);

            messageText = itemView.findViewById(R.id.text_message_body);
        }

        void bind(OwoMessage message) {
            messageText.setText(message.getMessage());
        }
    }
}
