package com.github.owo.owo.Utils;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import com.github.owo.owo.Activities.R;
import com.microsoft.identity.client.AuthenticationResult;
import com.microsoft.identity.client.PublicClientApplication;

public class AppSubClass extends AndroidViewModel {
    public AppSubClass(@NonNull Application app){
        super(app);
    }
    private PublicClientApplication app = null;
    private MutableLiveData<AuthenticationResult>  authResult =new MutableLiveData<AuthenticationResult>();

    private String watsonUsername=null;
    private String watsonPassword=null;
    private String watsonWorkspaceId=null;



    public MutableLiveData<AuthenticationResult> getAuthObserver(){
        return authResult;
    }

    public void setAuthResult(AuthenticationResult authResult) {
        getAuthObserver().setValue(authResult);
    }

    public AuthenticationResult getAuthResult(){
        return getAuthObserver().getValue();
    }

    public PublicClientApplication getApp() {
        return app;
    }

    public void setApp(PublicClientApplication app) {
        this.app = app;
    }
}
