package com.github.owo.owo.Models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Calendar;

public class EventModel implements Parcelable {
    private long id;
    private String title;
    private String type;
    private long calendarId;
    private Calendar startDate;
    private Calendar endDate;
    public static final Parcelable.Creator<EventModel> CREATOR= new Parcelable.Creator<EventModel>() {
        @Override
        public EventModel createFromParcel(Parcel parcel) {
            return new EventModel(parcel);
        }

        @Override
        public EventModel[] newArray(int i) {
            return new EventModel[i];
        }
    };
    public EventModel(long id,String title,String type, long calendarId, Calendar startDate, Calendar endDate){
        this.setId(id);
        this.setTitle(title);
        this.setType(type);
        this.setCalendarId(calendarId);
        this.setStartDate(startDate);
        this.setEndDate(endDate);
    }
    public EventModel(Calendar startDate, Calendar endDate){
        this(-1,"","",-1,startDate,endDate);
    }
    public EventModel(String title,String type, long calendarId, Calendar startDate, Calendar endDate){
        this(-1,title,type,calendarId,startDate,endDate);
    }
    public EventModel(Parcel parce){
        this(parce.readLong(),parce.readString(),parce.readString(),parce.readLong(),(Calendar) parce.readSerializable(),(Calendar)parce.readSerializable());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(getId());
        parcel.writeString(getTitle());
        parcel.writeString(getType());
        parcel.writeLong(getCalendarId());
        parcel.writeSerializable(getStartDate());
        parcel.writeSerializable(getEndDate());
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public long getCalendarId() {
        return calendarId;
    }

    public void setCalendarId(long calendarId) {
        this.calendarId = calendarId;
    }

    public Calendar getStartDate() {
        return startDate;
    }

    public void setStartDate(Calendar startDate) {
        this.startDate = startDate;
    }

    public Calendar getEndDate() {
        return endDate;
    }

    public void setEndDate(Calendar endDate) {
        this.endDate = endDate;
    }
}
