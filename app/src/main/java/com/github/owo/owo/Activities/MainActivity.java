package com.github.owo.owo.Activities;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.Network;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Toast;

import com.github.owo.owo.Adapters.MainPagerAdapter;
import com.github.owo.owo.Fragments.CalendarFragment;
import com.github.owo.owo.Fragments.FeedFragment;
import com.github.owo.owo.Interfaces.OwoEventManager;
import com.github.owo.owo.Models.EventModel;
import com.github.owo.owo.Services.OwoCalendarService;
import com.github.owo.owo.Utils.AppSubClass;
import com.github.owo.owo.Utils.NetworkChangeReceiver;
import com.microsoft.identity.client.AuthenticationCallback;
import com.microsoft.identity.client.AuthenticationResult;
import com.microsoft.identity.client.MsalException;
import com.microsoft.identity.client.PublicClientApplication;


public class MainActivity extends AppCompatActivity implements NetworkChangeReceiver.ConectionListener,OwoEventManager {

    public static String SP_EMAIL_KEY = "user_email";
    public static String SP_NAME_KEY = "user_name";
    public static String CLIENT_ID = "5301a170-53af-4a3f-a70a-a06a1dc9ff04";
    public static String[] SCOPES = new String[]{"https://graph.microsoft.com/User.Read"};
    private ViewPager viewPager;
    private boolean firstConection = false;
    private MainPagerAdapter pagerAdapter;
    private AppSubClass appModel;
    private ConnectivityManager.NetworkCallback networkCallback = new ConnectivityManager.NetworkCallback(){
        @Override
        public void onAvailable(Network net){
            if(!firstConection){
                firstConection=true;
                return;
            }
            super.onAvailable(net);
            runOnUiThread(new Runnable()
            {
                public void run()
                {
                    onConectionChange(true);
                }
            });
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initToolbar();
        initViewPager();
        initTabsNavigation();
        initOwoButton();
        initOwologin();


    }

    private void initToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    private void initViewPager() {
        viewPager = findViewById(R.id.pager);
        pagerAdapter = new MainPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(pagerAdapter);
    }

    private void initTabsNavigation() {
        TabLayout tabLayout = findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void initOwoButton() {
        FloatingActionButton owoButton = findViewById(R.id.owo);
        owoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent owoIntent = new Intent(MainActivity.this, MessageListActivity.class);
                MainActivity.this.startActivity(owoIntent);
            }
        });
    }
    private void initOwologin(){
        appModel = ViewModelProviders.of(this).get(AppSubClass.class);
        if(appModel.getAuthResult()==null){
            setUpUser();
        }
        showUserData();
    }

    private void setUpUser() {
        PublicClientApplication sampApp = appModel.getApp();
        try {
            if (sampApp == null) {
                sampApp = new PublicClientApplication(this.getApplicationContext(), CLIENT_ID);
                appModel.setApp(sampApp);
            }
            if (sampApp.getUsers().size() == 0) {
                startActivity(new Intent(MainActivity.this, Splash.class));
            }
            appModel.getApp().acquireTokenSilentAsync(SCOPES, (sampApp.getUsers()).get(0), getAuthSilentCallback());
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }
    private void showUserData(){
        appModel.getAuthObserver().observe(this, new Observer<AuthenticationResult>() {
            @Override
            public void onChanged(@Nullable AuthenticationResult auth) {
                if(auth!=null){
                    String nombre = appModel.getAuthResult().getUser().getName();
                    String correo = appModel.getAuthResult().getUser().getDisplayableId();
                    Log.e("SHIT USER", nombre+";"+correo);
                    Log.e("Credentials", appModel.getAuthResult().getIdToken());
                    Log.e("Credentials", appModel.getAuthResult().getAccessToken());
                    SharedPreferences shared = MainActivity.this.getPreferences(Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = shared.edit();
                    editor.putString(SP_NAME_KEY,nombre);
                    editor.putString(SP_EMAIL_KEY,correo);
                    editor.apply();
                }
            }
        });
    }
    private AuthenticationCallback getAuthSilentCallback(){
        return new AuthenticationCallback() {
            @Override
            public void onSuccess(AuthenticationResult authenticationResult) {

                Log.e("SHIT", "Successfully authenticated");
                Log.e("Auth result", authenticationResult.toString());
                appModel.setAuthResult(authenticationResult);
            }
            //@Override
            public void onError(MsalException exception) {
                Log.d("SHIT", "Authentication failed: " + exception.toString());
                Toast.makeText(getApplicationContext(), "No hay conexión a internet :/",Toast.LENGTH_LONG).show();
            }
            @Override
            public void onCancel() {
                /* User canceled the authentication */
                Log.d("SHIT", "User cancelled login.");
            }
        };
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public void onEventAdded() {
        runOnUiThread(new Runnable()
        {
            public void run()
            {
                FeedFragment ff = (FeedFragment) pagerAdapter.getItem(MainPagerAdapter.FEED);
                CalendarFragment cf = (CalendarFragment) pagerAdapter.getItem(MainPagerAdapter.CALENDARIO);
                cf.updateCursor();
                ff.getRecommendations();
            }
        });
    }

    @Override
    public void addEvent(EventModel event, ResultReceiver resultReceiver) {
        Intent addEventIntent = new Intent(this,OwoCalendarService.class);
        addEventIntent.setAction("com.github.owo.owo.Services.OwoCalendarService");
        addEventIntent.putExtra(OwoCalendarService.ACTION,OwoCalendarService.ADD_EVENT);
        SharedPreferences sharedPref = getPreferences(this.MODE_PRIVATE);
        String accountName = sharedPref.getString(MainActivity.SP_EMAIL_KEY, "");
        //String accountName="hola";
        addEventIntent.putExtra(OwoCalendarService.ACCOUNT_NAME_PARAM, accountName);
        addEventIntent.putExtra(OwoCalendarService.RESULT_RECEIVER, resultReceiver);
        addEventIntent.putExtra(OwoCalendarService.EVENT, event);
        Log.e("LLEGO","LLEGOOOOOOOOOOO");
        ComponentName s = getBaseContext().startService(addEventIntent);
    }

    @Override
    public void getRecomendedEvents(ResultReceiver resultReceiver) {
        Intent addEventIntent = new Intent(this,OwoCalendarService.class);
        addEventIntent.setAction("com.github.owo.owo.Services.OwoCalendarService");
        addEventIntent.putExtra(OwoCalendarService.ACTION,OwoCalendarService.GET_RECOMMENDED);
        SharedPreferences sharedPref = getPreferences(MainActivity.MODE_PRIVATE);
        String accountName = sharedPref.getString(MainActivity.SP_EMAIL_KEY, "");
        addEventIntent.putExtra(OwoCalendarService.ACCOUNT_NAME_PARAM, accountName);
        addEventIntent.putExtra(OwoCalendarService.RESULT_RECEIVER, resultReceiver);
        Log.e("LLEGO","LLEGOOOOOOOOOOO");
        getBaseContext().startService(addEventIntent);
    }

    @Override
    public void UpdateRecommended() {
        runOnUiThread(new Runnable()
        {
            public void run()
            {
                FeedFragment ff = (FeedFragment) pagerAdapter.getItem(MainPagerAdapter.FEED);
                ff.refreshRecommendations();
            }
        });
    }

    @Override
    public void onConectionChange(boolean isOniline) {

    }
}
