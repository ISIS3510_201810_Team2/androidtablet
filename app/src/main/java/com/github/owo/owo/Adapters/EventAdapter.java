package com.github.owo.owo.Adapters;

import android.database.Cursor;
import android.provider.CalendarContract;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.owo.owo.Activities.R;

import java.text.SimpleDateFormat;

public class EventAdapter extends RecyclerView.Adapter{
    private Cursor myCursor;
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.event_view,parent,false);
        return new EventViewHolder(v);
    }
    private String date2string(long value){
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy, HH:mm");
        return format.format(value);
    }
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        Log.e("EVENT-ADAPTER","JAJAJA saludos entre");
        if(myCursor!=null&&myCursor.moveToPosition(position)){
            Log.e("EVENT-ADAPTER","JAJAJA saludos entro if");
            int idName = myCursor.getColumnIndex(CalendarContract.Events.TITLE);
            int idDate = myCursor.getColumnIndex(CalendarContract.Events.DTSTART);
            int idType = myCursor.getColumnIndex(CalendarContract.Events.DESCRIPTION);
            String name  = myCursor.getString(idName);
            Long date = myCursor.getLong(idDate);
            String type = myCursor.getString(idType);
            ((EventViewHolder)holder).setName(name.substring(0,Math.min(25,name.length())));
            ((EventViewHolder)holder).setDate(date2string(date));
            if(type==null){
                type="";
            }
            type = type.replace(System.getProperty("line.separator"), " ");
            ((EventViewHolder)holder).setType(type.substring(0,Math.min(15,type.length())));

        }
    }

    @Override
    public int getItemCount() {
        return myCursor==null?0:myCursor.getCount();
    }
    @Override
    public long getItemId(int position){
        if(myCursor!=null&&myCursor.moveToPosition(position)){
            int idcol = myCursor.getColumnIndex(CalendarContract.Events._ID);
            long id = myCursor.getLong(idcol);
            return id;
        }
        return 0;
    }
    public Cursor getCursor(){
        return myCursor;
    }
    public void setCursor(Cursor curr){
        myCursor = curr;
        notifyDataSetChanged();
    }

}
