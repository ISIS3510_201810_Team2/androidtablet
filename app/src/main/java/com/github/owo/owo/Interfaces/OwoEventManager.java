package com.github.owo.owo.Interfaces;

import android.os.ResultReceiver;

import com.github.owo.owo.Models.EventModel;

public interface OwoEventManager {
    void onEventAdded();
    void addEvent(EventModel event, ResultReceiver resultReceiver);
    void getRecomendedEvents(ResultReceiver resultReceiver);
    void UpdateRecommended();
}
