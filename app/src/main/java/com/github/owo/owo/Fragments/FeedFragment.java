package com.github.owo.owo.Fragments;


import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.owo.owo.Activities.R;
import com.github.owo.owo.Adapters.RecommendationsAdapter;
import com.github.owo.owo.Interfaces.OwoEventManager;
import com.github.owo.owo.Models.EventModel;
import com.github.owo.owo.Services.OwoCalendarService;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class FeedFragment extends Fragment {

    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private ArrayList<EventModel> dataset = new ArrayList<>();
    private OwoEventManager manager;

    private final static String TAG = FeedFragment.class.getName();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_feed, container, false);

        initRecyclerView(view);

        return view;
    }

    private void initRecyclerView(View view) {
        Log.e(TAG, "initRecyclerView");

        recyclerView = view.findViewById(R.id.recommended_list);

        layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(layoutManager);

        adapter = new RecommendationsAdapter(dataset);
        recyclerView.setAdapter(adapter);

        getRecommendations();
    }

    public void setDataset(ArrayList<EventModel> newDataset) {
        Log.e(TAG, "setDataset");
        Log.e(TAG, "There are " + newDataset.size() + " items");

        dataset.clear();
        dataset.addAll(newDataset);
        manager.UpdateRecommended();
    }

    public void getRecommendations() {
        Log.e(TAG, "updateRecommendations");

        manager = (OwoEventManager) getContext();
        CreateTaskResponder crt = new FeedFragment.CreateTaskResponder(null);
        manager.getRecomendedEvents(crt);
    }

    public void refreshRecommendations() {
        Log.e(TAG, "refreshRecommendations");

        adapter.notifyDataSetChanged();
    }

    private class CreateTaskResponder extends ResultReceiver {

        public CreateTaskResponder(Handler handler) {
            super(handler);
        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle result) {
            if (resultCode == OwoCalendarService.OK_RESULT) {
                String action = result.getString(OwoCalendarService.ACTION);
                if (action.equals(OwoCalendarService.GET_RECOMMENDED)) {
                    Log.e(TAG, "getRecomendation");
                    ArrayList<EventModel> newDataset = result.getParcelableArrayList(OwoCalendarService.RECOMMENDED_RESULT);
                    FeedFragment.this.setDataset(newDataset);
                }
            } else {
                String errorString = result.getString(OwoCalendarService.ERROR_MESSAGE);
                Log.e(TAG, errorString);
            }
        }
    }

}
