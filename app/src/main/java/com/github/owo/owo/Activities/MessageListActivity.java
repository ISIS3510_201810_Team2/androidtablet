package com.github.owo.owo.Activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.github.owo.owo.Adapters.MessageListAdapter;
import com.github.owo.owo.Models.OwoMessage;
import java.util.ArrayList;
import java.util.List;

public class MessageListActivity extends AppCompatActivity {

    private RecyclerView messageListRecycler;
    private RecyclerView.Adapter messageListAdapter;

    private List<OwoMessage> messageList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message_list);

        final String testMessage = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam non egestas libero, id pulvinar amet.";
        messageList = new ArrayList<>();
        messageList.add(new OwoMessage(OwoMessage.MESSAGE_SENT, testMessage));
        messageList.add(new OwoMessage(OwoMessage.MESSAGE_RECEIVED, testMessage));
        messageList.add(new OwoMessage(OwoMessage.MESSAGE_SENT, testMessage));
        messageList.add(new OwoMessage(OwoMessage.MESSAGE_RECEIVED, testMessage));
        messageList.add(new OwoMessage(OwoMessage.MESSAGE_RECEIVED, testMessage));
        messageList.add(new OwoMessage(OwoMessage.MESSAGE_RECEIVED, testMessage));

        messageListRecycler = findViewById(R.id.recyclerview_message_list);
        messageListAdapter = new MessageListAdapter(this, messageList);
        messageListRecycler.setLayoutManager(new LinearLayoutManager(this));
        messageListRecycler.setAdapter(messageListAdapter);
    }
}
