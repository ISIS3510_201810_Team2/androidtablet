package com.github.owo.owo.Activities;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.github.owo.owo.Activities.LoginActivity;
import com.github.owo.owo.Activities.MainActivity;
import com.github.owo.owo.Activities.R;
import com.github.owo.owo.Utils.AppSubClass;
import com.microsoft.identity.client.MsalClientException;
import com.microsoft.identity.client.PublicClientApplication;
import com.microsoft.identity.client.User;

import java.util.List;

public class Splash extends AppCompatActivity {
    private String CLIENT_ID = "5301a170-53af-4a3f-a70a-a06a1dc9ff04";
    private String[] SCOPES = new String[]{"https://graph.microsoft.com/User.Read"};

    ///val TAG = "Splash activity"

    private AppSubClass appViewModel = null;

    /* Azure AD Variables */



    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        /* Configure your sample app and save state for this activity */

        appViewModel = ViewModelProviders.of(this).get(AppSubClass.class);
        PublicClientApplication sampleApp = appViewModel.getApp();

        /* Initialize the MSAL App context */
        if (sampleApp == null) {
            sampleApp = new PublicClientApplication(
                    getApplicationContext(),
                    CLIENT_ID);
            appViewModel.setApp(sampleApp);
        }

        /* Attempt to get a user and acquireTokenSilent
         * If this fails we do an interactive request
         */
        List<User> users = null;
        try {
            users = sampleApp.getUsers();
        } catch (MsalClientException e) {
            e.printStackTrace();
        }

        if (users != null && users.size() == 1) {
            /* We have 1 user */

            startActivity(new Intent(this, MainActivity.class));
            finish();
        } else {
            /* We have no user */
            /* Let's do an interactive request */
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
            finish();
        }

    }
}
