package com.github.owo.owo.Fragments;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.os.SystemClock;
import android.support.annotation.RequiresApi;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.github.owo.owo.Activities.R;
import com.github.owo.owo.Interfaces.OwoEventManager;
import com.github.owo.owo.Models.EventModel;
import com.github.owo.owo.Services.OwoCalendarService;

import java.util.ArrayList;
import java.util.Calendar;


public class CreateTaskDialog extends DialogFragment {
    private Context context= null;
    private long lastButtonMilli = SystemClock.elapsedRealtime();
    private  OwoEventManager manager = null;
    public  CreateTaskDialog(){
        // Required empty public constructor
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        context = null;
        manager = null;
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OwoEventManager) {
            Log.e("ATTACH","CREE EL MANAGER");
            manager = (OwoEventManager)context;
        }
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        final View rootView = inflater.inflate(R.layout.create_task_dialog_fragment, container);
        context = getActivity().getApplicationContext();
        ArrayAdapter<CharSequence> types_array = arrayAdapterFromId(R.array.task_types_array);
        final Spinner type_spinner = createSpinner(rootView, types_array, R.id.task_types_spinner);

        ArrayAdapter<CharSequence> years_array = arrayAdapterFromId(R.array.years_array);
        final Spinner year_spinner = createSpinner(rootView, years_array, R.id.task_year_spinner);

        ArrayAdapter<CharSequence> months_array = arrayAdapterFromId(R.array.months_array);
        final Spinner month_spinner = createSpinner(rootView, months_array, R.id.task_month_spinner);

        ArrayAdapter<CharSequence> days_array = arrayAdapterFromId(R.array.days_base_array);
        final Spinner days_spinner = createSpinner(rootView, days_array, R.id.task_day_spinner);

        ArrayAdapter<CharSequence> hours_array = arrayAdapterFromId(R.array.hours_array);
        final Spinner hours_spinner = createSpinner(rootView, hours_array, R.id.task_hour_spinner);

        ArrayAdapter<CharSequence> minutes_array = arrayAdapterFromId(R.array.minutes_array);
        final Spinner minutes_spinner = createSpinner(rootView, minutes_array, R.id.task_minutes_spinner);

        minutes_spinner.setSelection(Calendar.getInstance().get(Calendar.MINUTE));
        hours_spinner.setSelection(Calendar.getInstance().get(Calendar.HOUR_OF_DAY));
        int month = Calendar.getInstance().get(Calendar.MONTH);
        month_spinner.setSelection(Calendar.getInstance().get(Calendar.MONTH));
        final int year= Calendar.getInstance().get(Calendar.YEAR);
        year_spinner.setSelection(year - 2000);


        updateDaysArray(year, month + 1, days_spinner);

        // Set an on item selected listener for spinner object
        AdapterView.OnItemSelectedListener itemSelectedListener = new AdapterView.OnItemSelectedListener(){
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int year_value = year_spinner.getSelectedItemPosition()+2000;
                int month_value = month_spinner.getSelectedItemPosition();
                updateDaysArray(year_value, month_value + 1, days_spinner);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                //interface callback
            }
        };
        year_spinner.setOnItemSelectedListener(itemSelectedListener);
        month_spinner.setOnItemSelectedListener(itemSelectedListener);

        days_spinner.setSelection(Calendar.getInstance().get(Calendar.DAY_OF_MONTH) - 1);

        Button closeBtn = rootView.findViewById(R.id.close_create_task_button);
        closeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
                      public void onClick(View view) {
                dismiss();
            }
        });

        final TextView addTaskErrorText = rootView.findViewById(R.id.create_task_error_text);
        addTaskErrorText.setVisibility(View.GONE);
        Button addBtn = rootView.findViewById(R.id.add_task_button);
        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                long currMilli = SystemClock.elapsedRealtime();
                if (currMilli - lastButtonMilli >= 1000) {
                    lastButtonMilli = currMilli;
                    EditText text= rootView.findViewById(R.id.task_name_input);
                    String name = text.getText().toString();
                    int year = year_spinner.getSelectedItemPosition() + 2000;
                    int month = month_spinner.getSelectedItemPosition();
                    int day = days_spinner.getSelectedItemPosition();
                    int hour = hours_spinner.getSelectedItemPosition();
                    int minutes = minutes_spinner.getSelectedItemPosition();
                    String type = type_spinner.getSelectedItem().toString();
                    Calendar cal = Calendar.getInstance();
                    cal.set(year, month, day + 1, hour, minutes);
                    //Log.e("ADD", name);
                    //Log.e("ADD", cal.time.toString());
                    //Log.e("ADD", type);
                    if (name.equals("")) {
                        addTaskErrorText.setText("Completa todos los campos");
                        addTaskErrorText.setVisibility(View.VISIBLE);
                    } else {
                        Calendar endcal = Calendar.getInstance();
                        endcal.setTimeInMillis(cal.getTimeInMillis());
                        endcal.add(Calendar.HOUR_OF_DAY, 1);
                        EventModel e =new EventModel(name, type,-1, cal,endcal);

                        manager.addEvent(e, new CreateTaskResponder(null));
                    }
                }
            }
        });
        return rootView;
    }

    private void updateDaysArray(int year, int month, Spinner days_spinner){
        boolean isLeapYear = year % 4 == 0 && (year % 100 != 0 || year % 400 == 0);
        int add = 0;
        if(isLeapYear) {
            add = 1;
        }
        int daysInMonth = 31 - (month - 1) % 7 % 2;
        if (month == 2) {
            daysInMonth = 28 + add;
        }
        //val iter = (1..daysInMonth).iterator()
        ArrayList<CharSequence> arr = new ArrayList<CharSequence>();
        for (int i =1; i<=daysInMonth; ++i) {
            if (i <= 9) {
                arr.add("0" + i);
            } else {
                arr.add("" + i);
            }
        }
        days_spinner.setAdapter(new ArrayAdapter<CharSequence>(context, android.R.layout.simple_spinner_item, arr));

    }

    private ArrayAdapter<CharSequence> arrayAdapterFromId(int array_type){
        return ArrayAdapter.createFromResource(
                context,
                array_type,
                android.R.layout.simple_spinner_dropdown_item
        );
    }

    private Spinner createSpinner(View rootView, ArrayAdapter<CharSequence> adapter, int spinnerId){
        Spinner spinner = rootView.findViewById(spinnerId);
                spinner.setAdapter(adapter);
        return spinner;
    }
    private class CreateTaskResponder extends ResultReceiver {
        public CreateTaskResponder(Handler handler) {
            super(handler);
        }
        //@RequiresApi(api = Build.VERSION_CODES.M)
        @Override
        protected void onReceiveResult(int resultCode, Bundle result){
            if (resultCode == OwoCalendarService.OK_RESULT) {
                String action = result.getString(OwoCalendarService.ACTION);
                if (action.equals(OwoCalendarService.ADD_EVENT)) {
                    //Log.e("CreateTaskResponder", "added event :D")
                    Toast.makeText(getContext(), "Tarea agregada", Toast.LENGTH_LONG).show();
                    manager.onEventAdded();
                }
            } else {
                String errorString = result.getString(OwoCalendarService.ERROR_MESSAGE);
                Log.e("CreateTaskResponder", errorString);
            }
            CreateTaskDialog.this.dismiss();
        }
    }
}
