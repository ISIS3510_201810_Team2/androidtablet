package com.github.owo.owo.Adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.github.owo.owo.Fragments.AccountFragment;
import com.github.owo.owo.Fragments.CalendarFragment;
import com.github.owo.owo.Fragments.CampusFragment;
import com.github.owo.owo.Fragments.FeedFragment;

import java.util.ArrayList;
import java.util.List;

public class MainPagerAdapter extends FragmentPagerAdapter {

    public static final int FEED = 0;
    public static final int CALENDARIO = 1;
    public static final int CAMPUS = 2;
    public static final int ACCOUNT = 3;
    private final List<Fragment> fragmentList = new ArrayList<>();

    private final List<String> fragmentTitleList = new ArrayList<>();

    public MainPagerAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);

        addFragment("Inicio", new FeedFragment());
        addFragment("Calendario", new CalendarFragment());
        addFragment("Acceso a Campus", new CampusFragment());
        addFragment("Mi Cuenta", new AccountFragment());
    }

    public void addFragment(String fragmentTitle, Fragment fragment) {
        fragmentTitleList.add(fragmentTitle);
        fragmentList.add(fragment);
    }

    @Override
    public int getCount() {
        return fragmentList.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return fragmentTitleList.get(position);
    }

    @Override
    public Fragment getItem(int position) {
        return fragmentList.get(position);
    }
}
