package com.github.owo.owo.Models;

public class OwoMessage {

    public final static int MESSAGE_SENT = 1;
    public final static int MESSAGE_RECEIVED = 2;

    private int messageType;
    private String message;

    public OwoMessage(int messageType, String message) {
        this.messageType = messageType;
        this.message = message;
    }

    public int getMessageType() {
        return messageType;
    }

    public String getMessage() {
        return message;
    }

}
