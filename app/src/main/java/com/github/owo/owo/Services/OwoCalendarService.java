package com.github.owo.owo.Services;

import android.Manifest;
import android.app.IntentService;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.provider.CalendarContract;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.ViewTreeObserver;

import com.github.owo.owo.Models.EventModel;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.SimpleTimeZone;
import java.util.TimeZone;

public class OwoCalendarService extends IntentService {
    public static String ACTION = "action";
    public static String RESULT_RECEIVER = "result_receiver";
    public static String ADD_EVENT = "add_event";
    public static String GET_RECOMMENDED = "get_recomended";
    public static String EDIT_EVENT = "edit_event";
    public static String EVENT = "event";
    public static String RECOMMENDED_RESULT = "recommended_result";
    public static String CALENDAR_NAME = "Calendario OwO";
    public static String EXCHANGE_ACCOUNT_TYPE = "com.google.android.gm.exchange";
    public static String ACCOUNT_NAME_PARAM = "account_name";
    public static int OK_RESULT = 0;
    public static int ERROR_RESULT = 1;
    public static String ERROR_MESSAGE = "error_message";
    public static String PERMISSION_ERROR = "permission_error";
    public static int MORNING_MIN = 7;
    public static int EVENING_MAX = 22;
    private String accountName;
    public OwoCalendarService(){
      this("OwoCalendarService");
    }
    public OwoCalendarService(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        Log.e("LLEGO","LLEGOOOOOOOOOOOx2");
        Bundle extras = intent.getExtras();
        Log.e("LLEGOO",extras.toString());
        String action = extras.getString(ACTION);
        accountName = intent.getStringExtra(ACCOUNT_NAME_PARAM);
        ResultReceiver rr = intent.getParcelableExtra(RESULT_RECEIVER);
        Bundle resultData = new Bundle();
        resultData.putString(ACTION,action);
        if (ContextCompat.checkSelfPermission(getApplicationContext(),
                Manifest.permission.WRITE_CALENDAR)
                != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(getApplicationContext(),
                        Manifest.permission.READ_CALENDAR)
                        != PackageManager.PERMISSION_GRANTED) {
            resultData.putString(ERROR_MESSAGE, PERMISSION_ERROR);
            rr.send(ERROR_RESULT, resultData);
        }
        else if(action.equals(ADD_EVENT)){
            Log.e("LLEGO","LLEGOOOOOOOOOOOx4");
            EventModel eve = intent.getParcelableExtra(EVENT);
            if(eve!=null){
                eve.setCalendarId(getCalendarId());
                eve.setId(addEvent(eve));
                resultData.putParcelable(EVENT,eve);
                rr.send(OK_RESULT,resultData);
            }
            else{
                resultData.putString(ERROR_MESSAGE,"C MAMO event model is null");
                rr.send(ERROR_RESULT,resultData);
            }
        }
        else if(action.equals(GET_RECOMMENDED)){
            ArrayList<EventModel> events = getRecommendedEvents();
            resultData.putParcelableArrayList(RECOMMENDED_RESULT,events);
            rr.send(OK_RESULT,resultData);
        }

    }
    private ArrayList<EventModel> getEvents(String selection, String[] selectionArgs, String sortOrder){
        ArrayList<EventModel> events = new ArrayList<EventModel>();
        Cursor cur = null;
        ContentResolver cr = getContentResolver();

        String[] mProjection = new String[]{
                CalendarContract.Events._ID,
                CalendarContract.Events.TITLE,
                CalendarContract.Events.DESCRIPTION,
                CalendarContract.Events.CALENDAR_ID,
                CalendarContract.Events.DTSTART,
                CalendarContract.Events.DTEND};
        int id_index = 0;
        int title_index = 1;
        int description_index = 2;
        int calendar_id_index = 3;
        int dstart_index = 4;
        int dend_index = 5;
        Uri uri = CalendarContract.Events.CONTENT_URI;
        cur = cr.query(uri, mProjection,
                selection, selectionArgs,
                sortOrder);
        long id= 0;
        String title= "";
        String description = "";
        long calendarId= 0;
        Calendar startDate = java.util.Calendar.getInstance();
        Calendar endDate = java.util.Calendar.getInstance();
        while (cur != null && cur.moveToNext()) {
            id = cur.getLong(id_index);
            title = cur.getString(title_index);
            description = cur.getString(description_index);
            calendarId= cur.getLong(calendar_id_index);
            startDate.setTimeInMillis(cur.getLong(dstart_index));
            endDate.setTimeInMillis(cur.getLong(dend_index));
            events.add(new EventModel(id, title, description, calendarId, startDate, endDate));
            startDate = Calendar.getInstance();
            endDate = Calendar.getInstance();
        }
        return events;
    }

    private void fixToWakeHours(Calendar lastFreeTime) {
        if (lastFreeTime.get(Calendar.HOUR_OF_DAY) < MORNING_MIN) {
            lastFreeTime.set(Calendar.HOUR_OF_DAY, MORNING_MIN);
            lastFreeTime.set(Calendar.MINUTE, 0);
        } else if (lastFreeTime.get(Calendar.HOUR_OF_DAY) > EVENING_MAX) {
            lastFreeTime.add(Calendar.DATE, 1);
            lastFreeTime.set(Calendar.HOUR_OF_DAY, MORNING_MIN);
            lastFreeTime.set(Calendar.MINUTE, 0);
        }
    }

    private ArrayList<EventModel> getFreeTime() {
        Calendar today = java.util.Calendar.getInstance();
        Calendar fiveDays = java.util.Calendar.getInstance();
        fiveDays.add(java.util.Calendar.DATE, 5);
        String selection = "("+CalendarContract.Events.DTEND+" > ?) AND ("+CalendarContract.Events.DTSTART+" <= ?)";
        String[] selectionArgs = new String[] {today.getTimeInMillis()+"", fiveDays.getTimeInMillis()+""};
        String sortOrder = CalendarContract.Events.DTSTART+" ASC";
        ArrayList<EventModel> busyEvents = getEvents(selection, selectionArgs, sortOrder);

        Calendar lastFreeTime = java.util.Calendar.getInstance();
        ArrayList<EventModel> events = new ArrayList<EventModel>();
        Calendar calStart = java.util.Calendar.getInstance();
        Calendar calEnd = java.util.Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm");
        EventModel event;
        for (int i =0; i<busyEvents.size(); i++) {
            event = busyEvents.get(i);
            fixToWakeHours(lastFreeTime);
            Calendar startD = event.getStartDate();
            long secondsDiff = (startD.getTimeInMillis() - lastFreeTime.getTimeInMillis()) / 1000;
            int hoursDiff = (int)(secondsDiff / 3600);
            if (hoursDiff >= 1) {
                while (compareCalendarNoTime(lastFreeTime, startD) < 0) {
                    calStart.setTimeInMillis(lastFreeTime.getTimeInMillis());
                    calEnd.setTimeInMillis(calStart.getTimeInMillis());
                    calEnd.set(Calendar.HOUR_OF_DAY, EVENING_MAX);
                    events.add(new EventModel(calStart,calEnd));
                    lastFreeTime.setTimeInMillis(calEnd.getTimeInMillis());
                    lastFreeTime.add(Calendar.HOUR, 1);
                    fixToWakeHours(lastFreeTime);
                    calStart = Calendar.getInstance();
                    calEnd = Calendar.getInstance();
                }
                if (compareCalendarNoTime(lastFreeTime, startD) == 0) {
                    calStart.setTimeInMillis(lastFreeTime.getTimeInMillis());
                    calEnd.setTimeInMillis(startD.getTimeInMillis());
                    events.add(new EventModel(calStart,calEnd));
                    if (event.getEndDate().getTimeInMillis() > lastFreeTime.getTimeInMillis()) {
                        lastFreeTime.setTimeInMillis(event.getEndDate().getTimeInMillis());
                    }
                    calStart = Calendar.getInstance();
                    calEnd = Calendar.getInstance();
                }
            } else {
                if (event.getEndDate().getTimeInMillis() > lastFreeTime.getTimeInMillis()) {
                    lastFreeTime.setTimeInMillis(event.getEndDate().getTimeInMillis());
                }
            }
        }
        return events;
    }
    private int compareCalendarNoTime(Calendar c1,Calendar c2){
        if (c1.get(Calendar.YEAR) != c2.get(Calendar.YEAR))
            return c1.get(Calendar.YEAR) - c2.get(Calendar.YEAR);
        if (c1.get(Calendar.MONTH) != c2.get(Calendar.MONTH))
            return c1.get(Calendar.MONTH) - c2.get(Calendar.MONTH);
        else
            return c1.get(Calendar.DAY_OF_MONTH) - c2.get(Calendar.DAY_OF_MONTH);
    }


    private ArrayList<EventModel> getOwoEvents(){
        long calendarId = getCalendarId();
        Log.e("ACA VAMOS","QUE VERGA");
        String selection = "("+CalendarContract.Events.DTEND+" >= ?) AND " +
                "("+CalendarContract.Events.CALENDAR_ID +"= ?) AND " +
                "("+CalendarContract.Events.DESCRIPTION+" = 'Parcial' " +
                "OR " +CalendarContract.Events.DESCRIPTION+" = 'Tarea')";
        String[] selectionArgs = new String[]{
                java.util.Calendar.getInstance().getTimeInMillis()+"",
                calendarId+""};
        String sortOrder = CalendarContract.Events.DTSTART+" ASC LIMIT 5";
        return getEvents(selection, selectionArgs, sortOrder);
    }

    private ArrayList<EventModel>  getRecommendedEvents(){
        ArrayList<EventModel>  freeTimeEvents = getFreeTime();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm");
        ArrayList<EventModel>  owoEvents = getOwoEvents();
        ArrayList<EventModel>  events = new ArrayList<EventModel>();
        // Hashset O(1) operations vs TreeSet O(log n) operations
        HashSet<String> busyFreeTimes = new HashSet<String>();
        EventModel owoEvent;
        EventModel freeTime;
        for (int i = 0; i<owoEvents.size();++i) {
            owoEvent = owoEvents.get(i);
            for (int j = 0; j<freeTimeEvents.size(); ++j) {
                freeTime = freeTimeEvents.get(j);
                if (owoEvent.getStartDate().getTimeInMillis() >= freeTime.getEndDate().getTimeInMillis() &&
                        !busyFreeTimes.contains(freeTime.toString())) {
                    busyFreeTimes.add(freeTime.toString());
                    events.add(
                            new EventModel("Estudiar para "+owoEvent.getTitle(),
                                    "Estudiar",
                                    owoEvent.getCalendarId(),
                                    freeTime.getStartDate(),
                                    freeTime.getEndDate()));
                    break;
                }
            }
        }
        //Log.i(TAG, "done get recommended events")
        //for (e in events) {
         //   Log.i(TAG, "Recommend ${e.title}")
          //  Log.i(TAG, "From: ${dateFormat.format(e.startDate!!.time)}, To: ${dateFormat.format(e.endDate!!.time)}")
        //}
        return events;
    }

    private long addEvent(EventModel event) {
        ContentResolver cr = getContentResolver();
        long defaultTime = java.util.Calendar.getInstance().getTimeInMillis();
        ContentValues values = new ContentValues();
        values.put(CalendarContract.Events.TITLE, event.getTitle());
        values.put(CalendarContract.Events.DESCRIPTION, event.getType());
        values.put(CalendarContract.Events.CALENDAR_ID, event.getCalendarId());
        values.put(CalendarContract.Events.DTSTART, event.getStartDate().getTimeInMillis());
        values.put(CalendarContract.Events.DTEND, event.getStartDate().getTimeInMillis());
        values.put(CalendarContract.Events.EVENT_TIMEZONE, TimeZone.getDefault().toString());

        try {
            Uri uri = cr.insert(CalendarContract.Events.CONTENT_URI, values);
            return Long.parseLong(uri.getLastPathSegment());
        }
        catch (SecurityException e){
            return -1;
        }


    }

    private long getCalendarId() {
        Cursor cur = null;
        ContentResolver cr = getContentResolver();

        String[] mProjection = new String[]{CalendarContract.Calendars._ID};

        Uri uri = CalendarContract.Calendars.CONTENT_URI;
        String selection = "(("+CalendarContract.Calendars.ACCOUNT_NAME+" = ?) AND ("
                +CalendarContract.Calendars.ACCOUNT_TYPE
                + " = ?) AND ("
                +CalendarContract.Calendars.NAME+"= ?))";
        String[] selectionArgs = new String[]{accountName, EXCHANGE_ACCOUNT_TYPE, CALENDAR_NAME};

        // TODO("MOVE THIS SOMEWHERE")
        //if (ActivityCompat.checkSelfPermission(applicationContext, Manifest.permission.READ_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
        //  ActivityCompat.requestPermissions(ac, arrayOf(Manifest.permission.READ_CALENDAR), targetRequestCode)
        //}
        try {
            cur = cr.query(uri, mProjection, selection, selectionArgs, null);

            if (cur.moveToFirst()) {
                return cur.getLong(0);
            } else {
                return createCalendar();
            }
        }
        catch (SecurityException e){
            return -1;
        }

    }
    private long createCalendar(){
        ArrayList<String> x = new ArrayList<String>();
        ContentValues contentValues = new ContentValues();
        contentValues.put(CalendarContract.Calendars.ACCOUNT_NAME, accountName);
        contentValues.put(CalendarContract.Calendars.ACCOUNT_TYPE, EXCHANGE_ACCOUNT_TYPE);
        contentValues.put(CalendarContract.Calendars.NAME, CALENDAR_NAME);
        contentValues.put(CalendarContract.Calendars.CALENDAR_DISPLAY_NAME, CALENDAR_NAME);
        contentValues.put(CalendarContract.Calendars.CALENDAR_COLOR, Color.YELLOW);
        contentValues.put(CalendarContract.Calendars.VISIBLE, 1);
        contentValues.put(
                CalendarContract.Calendars.CALENDAR_ACCESS_LEVEL,
                CalendarContract.Calendars.CAL_ACCESS_OWNER);
        contentValues.put(CalendarContract.Calendars.OWNER_ACCOUNT, accountName);
        contentValues.put(
                CalendarContract.Calendars.ALLOWED_REMINDERS,
                "METHOD_ALERT, METHOD_EMAIL, METHOD_ALARM");
        contentValues.put(
                CalendarContract.Calendars.ALLOWED_ATTENDEE_TYPES,
                "TYPE_OPTIONAL, TYPE_REQUIRED, TYPE_RESOURCE");
        contentValues.put(
                CalendarContract.Calendars.ALLOWED_AVAILABILITY,
                "AVAILABILITY_BUSY, AVAILABILITY_FREE, AVAILABILITY_TENTATIVE");

        Uri uri = CalendarContract.Calendars.CONTENT_URI;
        uri = uri.buildUpon()
                .appendQueryParameter(android.provider.CalendarContract.CALLER_IS_SYNCADAPTER, "true")
                .appendQueryParameter(CalendarContract.Calendars.ACCOUNT_NAME, accountName)
                .appendQueryParameter(CalendarContract.Calendars.ACCOUNT_TYPE, EXCHANGE_ACCOUNT_TYPE).build();
        Uri ansUri = getContentResolver().insert(uri, contentValues);
        return Long.parseLong(ansUri.getLastPathSegment());
    }

}
