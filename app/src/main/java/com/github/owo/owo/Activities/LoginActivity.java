package com.github.owo.owo.Activities;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.github.owo.owo.Adapters.MainPagerAdapter;
import com.github.owo.owo.Utils.AppSubClass;
import com.github.owo.owo.Utils.NetworkChangeReceiver;
import com.microsoft.identity.client.AuthenticationCallback;
import com.microsoft.identity.client.AuthenticationResult;
import com.microsoft.identity.client.MsalException;
import com.microsoft.identity.client.PublicClientApplication;

public class LoginActivity extends AppCompatActivity implements NetworkChangeReceiver.ConectionListener {
    public static String CLIENT_ID = "5301a170-53af-4a3f-a70a-a06a1dc9ff04";
    public static String[] SCOPES = new String[]{"https://graph.microsoft.com/User.Read"};
    private boolean firstConection = false;
    private  AppSubClass appModel=null;
    private  PublicClientApplication myApp = null;
    private CardView loginButton= null;
    private CardView conectionIndicator=null;
    private TextView textConection=null;
    private Context myCon = null;
    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        loginButton = findViewById(R.id.login_button);
        conectionIndicator= findViewById(R.id.conection_indicator);
        textConection= conectionIndicator.findViewById(R.id.textConnection);
        myCon= this;
        appModel= ViewModelProviders.of(this).get(AppSubClass.class);
        myApp = new PublicClientApplication( this.getApplicationContext(), CLIENT_ID);
        appModel.setApp(myApp);
        loginButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                findViewById(R.id.pbLogin).setVisibility(View.VISIBLE);
                loginButton.setEnabled(false);
                Log.e("yei", "KHE PRCD");
                myApp.acquireToken(LoginActivity.this,SCOPES,LoginActivity.this.getAuthInteractiveCallback());
            }
        });

    }
    public  AuthenticationCallback getAuthInteractiveCallback(){
        return new AuthenticationCallback() {
            @Override
            public void onSuccess(AuthenticationResult authenticationResult) {

                PublicClientApplication sampleApp = appModel.getApp();
                Log.e("C MAMO", "llego aca.");
                /* Initialize the MSAL App context */
                if (sampleApp == null) {
                    sampleApp = new PublicClientApplication(
                            myCon,
                            CLIENT_ID);
                    appModel.setApp(sampleApp);
                }
                Log.e("yei", "successss");
                appModel.setAuthResult(authenticationResult);
                Intent intent = new Intent(myCon, MainActivity.class);
                startActivity(intent);
                finish();
            }
            //@Override
            public void onError(MsalException exception) {Log.e("C MAMO", "error.");}
             //   findViewById<ProgressBar>(R.id.pbLogin).visibility= View.GONE
              //  /* Failed to acquireToken */
               // Log.v(TAG, "Authentication failed: " + exception.toString())
                //loginButton?.isEnabled = true
//
  //              if (exception is MsalClientException) {
    //                Toast.makeText(applicationContext, "No hay conexión a internet :/", Toast.LENGTH_LONG).show()
      //          } else if (exception is MsalServiceException) {
        //            Toast.makeText(applicationContext, "Error de Microsoft, intente después :/", Toast.LENGTH_LONG).show()
          //      }
           // }
            @Override
            public void onCancel() {
               //(LoginActivity.this.findViewById<ProgressBar>(R.id.pbLogin).visibility= View.GONE
                /* User cancelled the authentication */
                Log.e("C MAMO", "User cancelled login.");
                loginButton.setEnabled(true);
            }
        };
    }
    @Override
    public void onConectionChange(boolean isOniline) {

    }
    @Override
    public void onActivityResult(int requestCode, int  resultCode,Intent data) {
        Log.e("Holi","Por fin");
        myApp.handleInteractiveRequestRedirect(requestCode, resultCode, data);
    }
}
