package com.github.owo.owo.Fragments;


import android.Manifest;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.provider.CalendarContract;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.CursorLoader;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.GridView;

import com.github.owo.owo.Activities.R;
import com.github.owo.owo.Adapters.EventAdapter;

import java.net.URI;
import java.util.Calendar;


/**
 * A simple {@link Fragment} subclass.
 */
public class CalendarFragment extends Fragment {

    private EventAdapter eventAdapter;
    private long lastButtonMilli  = SystemClock.elapsedRealtime();
    private String TAG = "Calendar";
    public CalendarFragment() {
        // Required empty public constructor
    }
    private void openCalendar() {
        long currMillis = java.util.Calendar.getInstance().getTimeInMillis();
        Uri.Builder builder = CalendarContract.CONTENT_URI.buildUpon();
        builder.appendPath("time");
        ContentUris.appendId(builder, currMillis);
        Intent intent = new Intent(Intent.ACTION_VIEW).setData(builder.build());
        startActivity(intent);

    }
    public void updateCursor(){
        if(askCalendarPermission()) return;
        Cursor cur = null;
        ContentResolver cr = requireActivity().getContentResolver();
        String[] proj = new String[]{CalendarContract.Events._ID,
                CalendarContract.Events.TITLE,
                CalendarContract.Events.DESCRIPTION,
                CalendarContract.Events.CALENDAR_ID,
                CalendarContract.Events.DTSTART,
                CalendarContract.Events.DTEND
        };
        String selection = "("+CalendarContract.Events.DTEND+" >= ?)";
        String [] selectionArgs = new String[]{""+Calendar.getInstance().getTimeInMillis()};
        Uri uri = CalendarContract.Events.CONTENT_URI;
        try {
            cur = cr.query(uri, proj,
                    selection, selectionArgs, CalendarContract.Events.DTSTART + " ASC");
            if (cur != null) {
                eventAdapter.setCursor(cur);
            }
        }
        catch (SecurityException e){

        }
    }
    private boolean askCalendarPermission(){
        FragmentActivity fa = requireActivity();
        boolean enters = false;
        if(ContextCompat.checkSelfPermission(fa,Manifest.permission.READ_CALENDAR)!=PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(fa,new String[]{Manifest.permission.READ_CALENDAR},0);
            enters = true;
        }
        if(ContextCompat.checkSelfPermission(fa,Manifest.permission.WRITE_CALENDAR)!=PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(fa,new String[]{Manifest.permission.WRITE_CALENDAR},0);
            enters = true;
        }
        return enters;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v =  inflater.inflate(R.layout.fragment_calendar, container, false);
        eventAdapter = new EventAdapter();
        boolean asked = askCalendarPermission();
        RecyclerView rv = v.findViewById(R.id.gridview);
        rv.setAdapter(eventAdapter);
        rv.setLayoutManager(new GridLayoutManager(getContext(), 4));
        updateCursor();
        final Button openCalendarButton =  v.findViewById(R.id.open_calendar_btn);
        openCalendarButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                openCalendar();
            }
        });
        final Button openCreateTaskButton  = v.findViewById(R.id.open_create_task);
        openCreateTaskButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                long currMilli = SystemClock.elapsedRealtime();
                if(currMilli-lastButtonMilli>=1000){
                    lastButtonMilli = currMilli;
                    //Log.e(TAG,"legueeeeeeeee");
                    CreateTaskDialog dialog = new CreateTaskDialog();
                    Bundle args = new Bundle();
                    args.putString("initFragment", TAG);
                    dialog.setArguments (args);
                    //Log.e(TAG,"legueeeeeeeee");
                    dialog.setTargetFragment(CalendarFragment.this, 1);
                    dialog.show(getFragmentManager(), "OwO Dialog");
                    //owoFab?.isEnabled= false
                    getFragmentManager().executePendingTransactions();

                    Dialog d = dialog.getDialog();
                    d.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                }
            }
        });

        return v;
    }

}
