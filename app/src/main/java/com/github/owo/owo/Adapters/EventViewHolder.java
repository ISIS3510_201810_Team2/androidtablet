package com.github.owo.owo.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.github.owo.owo.Activities.R;

public class EventViewHolder extends RecyclerView.ViewHolder {
    private TextView name;
    private TextView date;
    private TextView type;
    private ImageView image;
    public EventViewHolder(View itemView) {
        super(itemView);
        name = itemView.findViewById(R.id.name);
        date = itemView.findViewById(R.id.date);
        type = itemView.findViewById(R.id.type);
        image = itemView.findViewById(R.id.imageView);
    }
    public void setName(String name) {
        this.name.setText(name);
    }
    public void setDate(String date) {
        this.date.setText(date);
    }
    public void setType(String type) {
        this.type.setText(type);
        //TODO set image when setting type
    }


}
